variable "ssh_key" {
  type    = string
  default = "AWS_2022_05_12"
}

variable "aws_instance_count" {
  type    = number
  default = 1
}

variable "domain_name" {
  type    = string
  default = "mandryka.ru"
}

variable "install_nginx" {
  type    = string
  default = <<-EOF
    #!/bin/bash -xe
    sudo apt update -y
    sudo apt install nginx -y
    sudo sed -i 's/listen 80/listen 8080/g' /etc/nginx/sites-available/default
    sudo systemctl restart nginx
  EOF
}