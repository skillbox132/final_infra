provider "aws" {
  region = "us-east-1"
}

data "aws_ami" "os" {
  most_recent = true
  owners      = ["136693071363"]
  filter {
    name   = "name"
    values = ["debian-11-amd64-*"]
  }
}

data "aws_availability_zones" "available" {}

resource "aws_security_group" "app" {
  name = "Dynamic Security Group"
  dynamic "ingress" {
    for_each = ["22", "8080"]
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_default_subnet" "availability_zones" {
  availability_zone = data.aws_availability_zones.available.names[count.index]
  count             = var.aws_instance_count <= 1 ? 1 : 2
}

resource "aws_instance" "server" {
  ami                    = data.aws_ami.os.id
  instance_type          = "t2.micro"
  vpc_security_group_ids = [aws_security_group.app.id]
  key_name               = var.ssh_key
  user_data              = var.install_nginx
  subnet_id              = count.index % 2 == 0 ? aws_default_subnet.availability_zones[0].id : aws_default_subnet.availability_zones[1].id
  count                  = var.aws_instance_count
  tags                   = {
    Name = "final_server"
  }
  lifecycle {
    create_before_destroy = true
  }
  provisioner "local-exec" {
    command = "sleep 20 && ansible-playbook -i '${self.public_ip},' ../ansible/deploy_docker.yaml"
  }
}

resource "aws_elb_attachment" "attach_server" {
  elb      = aws_elb.app.id
  instance = aws_instance.server[count.index].id
  count    = var.aws_instance_count
}

resource "aws_elb" "app" {
  name               = "AppServer-Highly-Available-ELB"
  availability_zones = [data.aws_availability_zones.available.names[0], data.aws_availability_zones.available.names[1]]
  security_groups    = [aws_security_group.app.id]
  listener {
    lb_port           = 8080
    lb_protocol       = "http"
    instance_port     = 8080
    instance_protocol = "http"
  }
  health_check {
  healthy_threshold   = 5
  unhealthy_threshold = 10
  timeout             = 60
  target              = "HTTP:8080/"
  interval            = 70
}
}

resource "aws_route53_zone" "app" {
  name = "mandryka.ru"
}

resource "aws_route53_record" "app" {
  zone_id = aws_route53_zone.app.id
  name    = var.domain_name
  type    = "A"
  alias {
    name                   = aws_elb.app.dns_name
    zone_id                = aws_elb.app.zone_id
    evaluate_target_health = true
  }
}

output "dns_servers" {
  value = aws_route53_zone.app.name_servers
}
