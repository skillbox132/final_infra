# final_service

## О работе сценария
Terraform-сценарий репозитория производит развёртывание требуемой для работы [web-приложения](https://gitlab.com/skillbox132/final_service.git) инфраструктуры в AWS:
- 1 aws_elb;
- 2 aws_default_subnet;
- 1 aws_security_group;
- N-штук aws_instance, разделяя их по создаваемым подсетям aws_default_subnet и добавляет их к созданному aws_elb;
- aws_route53_zone и A-aws_route53_record для целевого домена.
Так же на создаваемых aws_instance производится:
- в рамках работы сценария terraform - первичная установка сервера nginx на порту HTTP:8080, с целью обеспечения работы сервиса Health Check;
- в рамках развёртывания роли ansible - установка программного обеспечения, необходимого для обеспечения работоспособности сервиса docker.

## Требования к окружению локального компьютера:
- [aws-cli - 2.6.4 или более поздняя версия](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html#getting-started-install-instructions);
- [terraform - v1.2.3 или более поздняя версия](https://www.terraform.io/downloads);
- [ansible - core 2.12.6 или более поздняя версия](https://docs.ansible.com/ansible/latest/installation_guide/index.html).
- [сопряжение aws-cli с аккаунтом AWS](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-quickstart.html);
- [сгенерированная пара SSH-ключей доступа для Amazon EC2 instance](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-key-pairs.html).

## Как развернуть инфраструктуру AWS:  
1. Склонировать репозиторий на локальный компьютер, следуя [этой инструкции](https://docs.github.com/en/github/creating-cloning-and-archiving-repositories/creating-a-repository-on-github/duplicating-a-repository#mirroring-a-repository-in-another-location).
2. Внести путь к своему приватному ключу доступа к AWS ec2 instance, скорректировав переменную private_key_file в файле terraform/ansible.cfg репозитория.
3. В файле terraform/variables.tf скорректировать переменные:
- ssh_key - наименование SSH-ключа в AWS;
- aws_instance_count - количество разворачиваемых aws_instance;
- domain_name - имя целевого домена.
4. Из контекста директории terraform репозитория выполнить команду:
   ```shell
   terraform init
   terraform apply --auto-approve
   ```
5. Список серверов из вывода сценария внести на площадке-регистраторе доменного имени.
6. Дождаться репликации доменного имнени, ппроверив командой:
   ```shell
   ping YOURDOMAIN.XX
   ```
7. Проверить доступность nginx по адресу http://<YOURDOMAIN.XX>:8080
